package com.hertzsystems;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

@Component
public class WebSocketHandlerImpl extends TextWebSocketHandler {
	private static final Logger LOG = LoggerFactory.getLogger(WebSocketHandlerImpl.class);

	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		LOG.info(session.getHandshakeHeaders().toString());
		super.afterConnectionEstablished(session);
	}

	@Override
	public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {
		LOG.info(message.getPayload().toString());
	}
}
