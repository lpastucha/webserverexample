package com.hertzsystems;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

@EnableWebSocket
@Configuration
@EnableAutoConfiguration
public class WebConfiguration extends SpringBootServletInitializer implements WebSocketConfigurer {

	@Autowired
	WebSocketHandlerImpl handler;

	@Override
	public void registerWebSocketHandlers(WebSocketHandlerRegistry register) {
		register.addHandler(handler, "/topic/");
	}

}
